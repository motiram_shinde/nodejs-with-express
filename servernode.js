var http = require("http");

http
  .createServer(function (req, res) {
    res.write("MY FIRST SERVER USING NODEJS");
    res.end();
  })
  .listen(4000);
