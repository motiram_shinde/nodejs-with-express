const express = require("express");

const app = express();
app
  .get("/", function (req, res) {
    res.send("This is my first server created using express.");
  })
  .listen(4000);
