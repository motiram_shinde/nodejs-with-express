// var http = require("http");

// http
//   .createServer((req, res) => {
//     if (req.url === "/") {
//       res.write("Hello");
//       res.end();
//     }
//     if (req.url === "/api/course") {
//       res.write(JSON.stringify([1, 2, 3]));
//       res.end();
//     }
//   })
//   .listen(3000);

// console.log("listening on port 3000....");
const Joi = require("joi");
const express = require("express");
const { required } = require("joi");
const res = require("express/lib/response");

const app = express();

app.use(express.json());

const courses = [
  {
    id: 1,
    name: "coures1",
  },
  {
    id: 2,
    name: "coures2",
  },
  {
    id: 3,
    name: "coures3",
  },
];

app.get("/", (req, res) => {
  res.send("Hello World");
});

app.get("/api/courses", (req, res) => {
  res.send(courses);
});

app.get("/api/courses/:id", (req, res) => {
  const course = courses.find((c) => c.id == parseInt(req.params.id));
  if (!course) {
    res.status(404).send("The course with the given id not found");
  }
  res.send(course);
});

app.post("/api/courses", (req, res) => {
  if (!req.body.name || req.body.name.length < 3) {
    res.status(400).send("Name is required and shuld be minimum 3 character");
  }
  if (req.body.name.length >= 3) {
    const course = {
      id: courses.length + 1,
      name: req.body.name,
    };
    courses.push(course);
    res.send(course);
  }
});
//PORT

app.delete("/api/courses/:id", (req, res) => {
  const course = courses.find((c) => c.id == parseInt(req.params.id));
  if (!course) {
    res.status(400).send("Coures with given id is not present");
  }
  if (course) {
    const index = courses.indexOf(course);
    courses.slice(index, 1);

    res.send(course);
  }
});
const port = process.env.PORT || 5000;
app.listen(port, () => console.log(`Listening on port ${port}....`));
