var express = require("express");

var bodyParser = require("body-parser");

const PORT = 3000;

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));

let bookList = [
  "Make Time: How to Focus on what Matters Every Day",
  "The Power Of Habit",
];

app.listen(PORT, () =>
  console.log(`The Books API is running on: http://localhost:${PORT}.`)
);

app.get("/", (request, response) => {
  response.send("Welcome on the books API! Take a breath and start using it!");
});

app.get("/booksapi", (request, response) => {
  return response.json({ allBooks: bookList });
});

app.post("/booksapi", (request, response) => {
  const bookToAdd = request.body.name;

  if (bookList.includes(bookToAdd)) return response.json({ success: false });

  bookList.push(bookToAdd);
  return response.json({ success: true });
});

app.delete("/booksapi", (request, response) => {
  const bookToDelete = request.body.name;

  bookList = bookList.filter((book) => book !== bookToDelete);

  return response.json({ allBooks: bookList });
});

app.put("/booksapi", (request, response) => {
  const bookToUpdate = request.body.nameToUpdate;
  const updatedBook = request.body.updatedName;

  const indexOfBookToUpdate = bookList.findIndex(
    (book) => book === bookToUpdate
  );

  if (indexOfBookToUpdate === -1) return response.json({ success: false });

  bookList[indexOfBookToUpdate] = updatedBook;
  return response.json({ success: true });
});
